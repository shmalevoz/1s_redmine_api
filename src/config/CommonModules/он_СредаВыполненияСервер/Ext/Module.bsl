﻿//
// Серверные методы среды выполнения
// 

// Возвращает код среды выполнения
//
// Параметры: 
// 	СредаВыполнения
//
// Возвращаемое значение:
//   Число
//
Функция Код(СредаВыполнения) Экспорт
	
	Возврат Перечисления.он_СредыВыполнения.Индекс(СредаВыполнения);
	
КонецФункции //Код 

// Возвращает результат поиска по коду среды исполнения. Обратна к Код()
//
// Параметры: 
// 	Код
//
// Возвращаемое значение:
//   Перечисление.л4с_СредыВыполнения
//
Функция НайтиПоКоду(Код) Экспорт
	
	Возврат Перечисления.он_СредыВыполнения.Получить(Код);
	
КонецФункции //НайтиПоКоду 