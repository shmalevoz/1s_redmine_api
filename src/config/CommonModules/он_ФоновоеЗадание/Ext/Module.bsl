﻿//
// Серверные методы фоновых заданий
// 

// Возвращает постфикс обрамления имени метода в ключе задания
//
// Параметры:  
//
// Возвращаемое значение: 
// 	Строка
//
Функция КлючМетодПостфикс() Экспорт
	
	Возврат "__ИмяМетодаОкончание__";
	
КонецФункции //КлючМетодПостфикс 

// Возвращает имя метода из ключа процесса
//
// Параметры:  
//
// Возвращаемое значение: 
//
Функция КлючМетодИмя(Ключ)
	
	КлючЧасти	= он_Строка.ВМассив(Ключ, КлючМетодПостфикс());
	
	Возврат ?(КлючЧасти.Количество() > 1, КлючЧасти[0], "");
	
КонецФункции //КлючМетодИмя 

// Возвращает массив идентификаторов фоновых заданий по фильтру
//
// Параметры:  
// 	Отбор - см. ПолучитьФоновыеЗадания
//
// Возвращаемое значение: 
// 	Массив
//
Функция Получить(Отбор) Экспорт
	
	Результат	= Новый Массив;
	
	Фильтр	= он_Коллекции.СтруктураСкопировать(Отбор);
	Метод	= "";
	Если Фильтр.Свойство("ИмяМетода") Тогда
		Метод	= НРег(Фильтр.ИмяМетода);
		Фильтр.Удалить("ИмяМетода");
	КонецЕсли;
	
	// Переписываем процессы в результат
	Для Каждого Процесс Из ФоновыеЗадания.ПолучитьФоновыеЗадания(Фильтр) Цикл
		Если ПустаяСтрока(Метод) 
			ИЛИ НРег(Процесс.ИмяМетода) = Метод 
			ИЛИ НРег(КлючМетодИмя(Процесс.Ключ)) = Метод Тогда
			Результат.Добавить(Процесс.УникальныйИдентификатор);
		КонецЕсли;
	КонецЦикла;
	
	Возврат Результат;
	
КонецФункции //Найти 

// Истина, если задание с указанным методом активно
//
// Параметры:  
// 	Метод - имя метода задания
//
// Возвращаемое значение: 
// 	Булево
//
Функция Выполняется(Метод) Экспорт
	
	Возврат Получить(Новый Структура("Состояние, ИмяМетода"
	, СостояниеФоновогоЗадания.Активно
	, Метод)).Количество() <> 0;
	
КонецФункции //Выполняется 

// Возвращает описаание созданного фонового задания
//
// Параметры:  
// 	Метод
// 	Параметры:Массив - массив параметров вызываемого метода
// 	Имя - наименование задания
// 	Уникальность - уникальность выполнения 
//
// Возвращаемое значение: 
// 	Структура - описание созданного фонового задания
//
Функция Создать(Метод, Параметры = Неопределено, Имя = Неопределено, Уникальность = Ложь) Экспорт
	
	Возврат он_ФоновоеЗаданиеСервер.Создать(Метод, Параметры, Имя, Уникальность);
	
КонецФункции //Создать 

// Возвращает фоновое задание по его описанию
//
// Параметры:  
// 	Задание
//
// Возвращаемое значение: 
// 	ФоновоеЗадание
//
Функция ПроцессПолучить(Задание)
	
	Возврат ФоновыеЗадания.НайтиПоУникальномуИдентификатору(Задание);
	
КонецФункции //ПроцесПолучить 

// Возвращает состояние задания
//
// Параметры:  
// 	Задание
//
// Возвращаемое значение: 
// 	СостояниеФоновогоПроцесса|Неопределено
//
Функция ЗаданиеСостояние(Задание)
	
	Процесс	= ПроцессПолучить(Задание);
	
	Возврат ?(Процесс = Неопределено
	, Неопределено
	, Процесс.Состояние);
	
КонецФункции //ЗаданиеСостояние 

// Истина, если фоновое задание активно
//
// Параметры:  
// 	Задание
//
// Возвращаемое значение: 
// 	Булево
//
Функция Активно(Задание) Экспорт
	
	Возврат ЗаданиеСостояние(Задание) = СостояниеФоновогоЗадания.Активно;
	
КонецФункции //Активно 

// Истина, если фоновое задание завершено
//
// Параметры:  
// 	Задание
//
// Возвращаемое значение: 
// 	Булево
//
Функция Завершено(Задание) Экспорт
	
	Возврат ЗаданиеСостояние(Задание) = СостояниеФоновогоЗадания.Завершено;
	
КонецФункции //Завершено 

// Истина, если фоновое задание завершено с ошибкой
//
// Параметры:  
// 	Задание
//
// Возвращаемое значение: 
// 	Булево
//
Функция ЗавершеноАварийно(Задание) Экспорт
	
	Возврат ЗаданиеСостояние(Задание) = СостояниеФоновогоЗадания.ЗавершеноАварийно;
	
КонецФункции //ЗавершеноАварийно 

// Истина, если фоновое задание отменено
//
// Параметры:  
// 	Задание
//
// Возвращаемое значение: 
// 	Булево
//
Функция Отменено(Задание) Экспорт
	
	Возврат ЗаданиеСостояние(Задание) = СостояниеФоновогоЗадания.Отменено;
	
КонецФункции //Отменено 

// Прерывает выполнение фонового задания
//
// Параметры:  
// 	Задание
//
Процедура Отменить(Задание) Экспорт
	
	Процесс	= ПроцессПолучить(Задание);
	
	Если Процесс <> Неопределено Тогда
		Процесс.Отменить();
	КонецЕсли;
	
КонецПроцедуры //Отменить 

// Ожидание завершения задания
//
// Параметры:  
// 	Задание
// 	Таймаут
//
Процедура ЗавершениеОжидать(Задание, Таймаут = Неопределено) Экспорт
	
	Процесс	= ПроцессПолучить(Задание);
	
	Если Процесс <> Неопределено Тогда
		Процесс.ОжидатьЗавершения(Таймаут);
	КонецЕсли;
	
КонецПроцедуры //ЗавершениеОжидать 

// Возвращает начало выполнения задания
//
// Параметры:  
// 	Задание
//
// Возвращаемое значение: 
// 	Дата|Неопределено
//
Функция Начало(Задание) Экспорт
	
	Процесс	= ПроцессПолучить(Задание);
	
	Возврат ?(Процесс = Неопределено, Неопределено, Процесс.Начало);
	
КонецФункции //Начало 

// Возвращает дату окончания выполнения фонового задания
//
// Параметры:  
// 	Задание
//
// Возвращаемое значение: 
// 	Дата|Неопределено
//
Функция Окончание(Задание) Экспорт
	
	Процесс	= ПроцессПолучить(Задание);
	
	Возврат ?(Процесс = Неопределено, Неопределено, Процесс.Конец);
	
КонецФункции //Окончание 

// Возвращает информацию об ошибке выполнения задания
//
// Параметры:  
// 	Задание
//
// Возвращаемое значение: 
// 	ИнформацияОбОшибке|Неопределено
//
Функция ОшибкаИнформация(Задание) Экспорт
	
	Процесс	= ПроцессПолучить(Задание);
	
	Возврат ?(Процесс = Неопределено, Неопределено, Процесс.ИнформацияОбОшибке);
	
КонецФункции //ОшибкаИнформация 

// Возвращает связанное регламентное задание
//
// Параметры:  
// 	Задание
//
// Возвращаемое значение: 
// 	РегламентноеЗадание|Неопределено
//
Функция РегламентноеЗадание(Задание) Экспорт
	
	Процесс	= ПроцессПолучить(Задание);
	
	Возврат ?(Процесс = Неопределено, Неопределено, Процесс.РегламентноеЗадание);
	
КонецФункции //РегламентноеЗадание 

// Возвращает точку выполнения задания
//
// Параметры:  
// 	Задание
//
// Возвращаемое значение: 
// 	Строка|Неопределено
//
Функция Расположение(Задание) Экспорт
	
	Процесс	= ПроцессПолучить(Задание);
	
	Возврат ?(Процесс = Неопределено, Неопределено, Процесс.Расположение);
	
КонецФункции //Расположение 

// Возвращает сообщения фонового задания
//
// Параметры:  
// 	Задание
// 	Удалить
//
// Возвращаемое значение: 
// 	ФиксированныйМассив|Неопределено
//
Функция Сообщения(Задание, Удалить = Истина) Экспорт
	
	Процесс	= ПроцессПолучить(Задание);
	
	Возврат ?(Процесс = Неопределено, Неопределено, Процесс.ПолучитьСообщенияПользователю(Удалить));
	
КонецФункции //Сообщения 

// Возвращает идентификатор итератора сообщения
//
// Параметры:  
//
// Возвращаемое значение: 
// 	Уникальный идентификатор
//
Функция ПрогрессОповещениеИдентификатор()
	
	Возврат Новый УникальныйИдентификатор("84ebf67c-d67b-464d-8bb2-65894a276a66");
	
КонецФункции //ПрогрессОповещениеИдентификатор 

// Возвращает идентификатор оповещения о результате выполнения задания
//
// Параметры:  
//
// Возвращаемое значение: 
// 	УникальныйИдентификатор
//
Функция РезультатОповещениеИдентификатор()
	
	Возврат Новый УникальныйИдентификатор("fd97b86e-92dc-4f7d-a536-6517795e47e2");
	
КонецФункции //РезультатОповещениеИдентификатор 

// Истина, если сообщения содержит данные итератора
//
// Параметры:  
// 	Сообщение
//
// Возвращаемое значение: 
// 	Булево
//
Функция СообщениеПрогрессОповещениеЭто(Сообщение)
	
	Возврат Сообщение.ИдентификаторНазначения = ПрогрессОповещениеИдентификатор();
	
КонецФункции //СообщениеПрогрессОповещениеЭто 

// Истина, если сообщение содержит результат выполнения фонового задания
//
// Параметры:  
// 	Сообщение
//
// Возвращаемое значение: 
// 	Булево
//
Функция СообщениеРезультатВыполненияЭто(Сообщение)
	
	Возврат Сообщение.ИдентификаторНазначения = РезультатОповещениеИдентификатор();
	
КонецФункции //СообщениеРезультатВыполненияЭто 

// Устанавливает значение прогреса выполнения фонового задания
//
// Параметры:  
// 	Значение
//
Процедура ПрогрессСообщить(Значение, МинимальноеЗначение = 0, МаксимальноеЗначение = 100, ВыполнениеПрефикс = "Выполнено") Экспорт
	
	Сообщение	= Новый СообщениеПользователю;
	Сообщение.ИдентификаторНазначения	= ПрогрессОповещениеИдентификатор();
	Сообщение.Текст						= он_ОбъектыПрикладные.Сериализовать(Новый Структура("Значение, МинимальноеЗначение, МаксимальноеЗначение, ВыполнениеПрефикс"
	, Значение
	, МинимальноеЗначение
	, МаксимальноеЗначение
	, ВыполнениеПрефикс));
	Сообщение.Сообщить();
	
КонецПроцедуры //ПрогрессСообщить

// Устанавливает результат выполнения процесса
//
// Параметры:  
// 	Значение
//
Процедура РезультатСообщить(Значение) Экспорт
	
	Сообщение	= Новый СообщениеПользователю;
	Сообщение.ИдентификаторНазначения	= РезультатОповещениеИдентификатор();
	Сообщение.Текст = он_ОбъектыПрикладные.Сериализовать(Значение);
	Сообщение.Сообщить(); 
	
КонецПроцедуры //РезультатСообщить 

// Возвращает значение прогресса и результата выполнения фонового задания
//
// Параметры:  
// 	Задание
//
// Возвращаемое значение: 
// 	Число - значение прогресса
//
Функция ВыполнениеПолучить(Задание) Экспорт
	
	Результат		= Неопределено;
	СообщенияМассив	= Сообщения(Задание);
	
	Если СообщенияМассив <> Неопределено Тогда
		Результат	= Новый Структура("Прогресс, Результат"
		, Неопределено
		, Новый Массив);
		Для Каждого Источник Из СообщенияМассив Цикл
			Если СообщениеПрогрессОповещениеЭто(Источник) Тогда
				Результат.Прогресс	= он_ОбъектыПрикладные.Десериализовать(Источник.Текст);
			ИначеЕсли СообщениеРезультатВыполненияЭто(Источник) Тогда
				Результат.Результат.Добавить(он_ОбъектыПрикладные.Десериализовать(Источник.Текст));
			Иначе
				// транслируем сообщения дальше
				Сообщение	= Новый СообщениеПользователю;
				Сообщение.ИдентификаторНазначения	= Источник.ИдентификаторНазначения;
				Сообщение.ПутьКДанным				= Источник.ПутьКДанным;
				Сообщение.КлючДанных				= Источник.КлючДанных;
				Сообщение.Поле						= Источник.Поле;
				Сообщение.Текст						= Источник.Текст;
				Сообщение.Сообщить();
			КонецЕсли;
		КонецЦикла;
	КонецЕсли;
	
	Возврат Результат;
	
КонецФункции //ВыполнениеПолучить 